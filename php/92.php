<?php

$a = 5;
$b = 4;

echo "<p>$a sama dengan $b? " . ($a == $b ? 'Ya' : 'Tidak') . "</p>";

echo "<p>$a tidak sama dengan $b? " . ($a != $b ? 'Ya' : 'Tidak') . "</p>";

echo "<p>$a lebih besar dari $b? " . ($a > $b ? 'Ya' : 'Tidak') . "</p>";

echo "<p>$a lebih kecil dari $b? " . ($a < $b ? 'Ya' : 'Tidak') . "</p>";

echo "<p>Apakah ($a sama dengan $b) DAN ($a lebih besar dari $b)? " . (($a == $b) && ($a > $b) ? 'Ya' : 'Tidak') . "</p>";

echo "<p>Apakah ($a sama dengan $b) ATAU ($a lebih besar dari $b)? " . (($a == $b) || ($a > $b) ? 'Ya' : 'Tidak') . "</p>";

?>
