<?php

$gaji = 1000000; 
$pajak = 0.1;
$thp = $gaji * (1 - $pajak); 

$gaji_format = number_format($gaji, 2, ',', '.'); 
$thp_format = number_format($thp, 2, ',', '.');

echo "<style>";
echo "body {";
echo "background-image: url('download.jpeg');";
echo "}";
echo "</style>";

echo "<div style='padding: 10px;'>";
echo "<div style='display: inline-block; width: 50%;'>";
echo "<span style='color: blue;'>Gaji sebelum dipotong pajak adalah Rp. $gaji_format</span><br>";
echo "</div>";
echo "<div style='display: inline-block; width: 50%;'>";
echo "<span style='color: green;'>Setelah dipotong pajak sebesar 10%, gaji bersih yang dibawa pulang adalah Rp. $thp_format</span>.";
echo "</div>";
echo "</div>";

?>
