<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $tinggi = $_POST['tinggi'];

    if (!is_numeric($tinggi) || $tinggi <= 0 || $tinggi != intval($tinggi)) {
        echo "Input tidak valid. Masukkan bilangan bulat positif.\n";
        exit;
    }

    echo "<h2>Segitiga Bintang Segitiga Sama Sisi dengan Tinggi $tinggi</h2>";
    for ($i = 1; $i <= $tinggi; $i++) {
        for ($j = $tinggi - $i; $j > 0; $j--) {
            echo "&nbsp;&nbsp;";
        }
        for ($k = 1; $k <= 2 * $i - 1; $k++) {
            echo "*";
        }
        echo "<br>";
    }
} else {
    echo "Akses tidak valid.";
}

?>
