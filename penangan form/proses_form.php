<!DOCTYPE html>
<html>
<head>
    <title>Proses Formulir PHP</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            background-color: #f9f9f9;
            box-shadow: 0 0 10px rgba(0,0,0,0.1);
        }
        h2 {
            color: #333;
        }
        p {
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
    <h2>Terima Kasih!</h2>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $nama = $_POST['nama'];
        $pesan = $_POST['pesan'];
        echo "<p>Halo, $nama!</p>";
        echo "<p>Terima kasih atas pesan Anda:</p>";
        echo "<p><em>$pesan</em></p>";
    } else {
        echo "<p>Maaf, tidak ada data yang dikirimkan.</p>";
    }
    ?>
    <p><a href="form.html">Kembali ke Formulir</a></p>
</body>
</html>
