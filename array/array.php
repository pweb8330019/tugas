<?php

function cariBuku($daftarBuku, $judul) {
    foreach ($daftarBuku as $buku) {
        if ($buku['judul'] == $judul) {
            return $buku;
        }
    }
    return null;
}
 
function urutkanBukuTahunTerbit($daftarBuku) {
    usort($daftarBuku, function($a, $b) {
        return $a['tahun_terbit'] - $b['tahun_terbit'];
    });
    return $daftarBuku;
}

$daftarBuku = array(
    array('judul' => 'Pemrograman PHP', 'penulis' => 'budi', 'tahun_terbit' => 2015),
    array('judul' => 'Dasar-Dasar HTML', 'penulis' => 'joko', 'tahun_terbit' => 2018),
    array('judul' => 'Python untuk Pemula', 'penulis' => 'agus', 'tahun_terbit' => 2017),
    array('judul' => 'JavaScript Lanjutan', 'penulis' => 'maul', 'tahun_terbit' => 2016),
);

echo "<h2>Daftar Buku Sebelum Diurutkan</h2>";
echo "<ul>";
foreach ($daftarBuku as $buku) {
    echo "<li>{$buku['judul']} - {$buku['penulis']} ({$buku['tahun_terbit']})</li>";
}
echo "</ul>";

$judulCari = 'Python untuk Pemula';
$bukuDitemukan = cariBuku($daftarBuku, $judulCari);

if ($bukuDitemukan) {
    echo "<h2>Detail Buku yang Dicari</h2>";
    echo "<p>Judul: {$bukuDitemukan['judul']}</p>";
    echo "<p>Penulis: {$bukuDitemukan['penulis']}</p>";
    echo "<p>Tahun Terbit: {$bukuDitemukan['tahun_terbit']}</p>";
} else {
    echo "<p>Buku dengan judul '$judulCari' tidak ditemukan.</p>";
}

$daftarBukuUrut = urutkanBukuTahunTerbit($daftarBuku);

echo "<h2>Daftar Buku Setelah Diurutkan</h2>";
echo "<ul>";
foreach ($daftarBukuUrut as $buku) {
    echo "<li>{$buku['judul']} - {$buku['penulis']} ({$buku['tahun_terbit']})</li>";
}
echo "</ul>";

?>
